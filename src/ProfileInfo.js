import React from 'react'
import FakeEntryField from './FakeEntryField';

// components
import ProfileCard from './ProfileCard';

class ProfileInfo extends React.Component {
	render(){
		const styles = {
  			container: {
    			background: "white",
  				borderRadius: "50px",
  				width: "95%",
  				minHeight: "90%",
					paddingTop: '5px',
					margin: 'auto',
  			},
  			
  			cardContainer: {
					margin: "20px",
					fontSize: "30px",
  			},

  			informationContainer: {
					boxSizing: 'border-box',
					padding: '10px',
  				display: "flex",
					flexFlow: 'row wrap',
					justifyContent: 'space-around',
  			},
  			
  			aboutMe: {
					width: '45%',
					height: '400px',
  				borderWidth: "2px",
  				borderStyle: "solid",
  				borderColor: "black",
  				textAlign: "left",
  				fontSize: "20px",
  				fontWeight: "bold",
  				paddingLeft: "10px",
  			},

				aboutMeInfo: {
					margin: '10px',
				},

				imagesWrapper: {
					width: '100%',
				},

  			pictureInfo: {
  				width: "50%",
  				marginLeft: "20px",
  				textAlign: "left",
  				fontSize: "20px",
  				fontWeight: "bold",
  				marginTop: "50px",
  				paddingLeft: "10px",
  			},

				images: {
					width: "90%",
					maxWidth: '240px',
					height: "auto",
					margin: '12px',
				},

				myDetails: {
					width: "40%",
					borderWidth: "2px",
					borderStyle: "solid",
					textAlign: "left",
					fontSize: "20px",
					fontWeight: "bold",
					paddingLeft: "10px",
				},

				detailsLabel: {
					fontSize: "30px",
				},

				list: {
					lineHeight: "60px",
					marginLeft: "20px",
				}
	}

	return(
		<div style={styles.container}>

			<div style={styles.cardContainer}>	
				<ProfileCard profile={this.props.profile} />
			</div>
			
			<div style={styles.informationContainer}>
			{/*About Me Section*/}
			<div style={styles.aboutMe}>
				<FakeEntryField size='big' label='About Me: ' content={this.props.profile.about} />
			</div>
			
			{/*My Details Section*/}
			<div style={styles.myDetails}>
				<p style={styles.detailsLabel}>My Details</p>
				<ul style={styles.list}>
					<li style={styles.list}><FakeEntryField size='small' label='Height: ' content={this.props.profile.height} /></li>
					<li style={styles.list}><FakeEntryField size='small' label='Birthday: ' content={this.props.profile.birthday} /></li>
					<li style={styles.list}><FakeEntryField size='small' label='Diet: ' content={this.props.profile.diet} /></li>
					<li style={styles.list}><FakeEntryField size='small' label='Interested in: ' content={this.props.profile.interested} /></li>
				</ul>
			</div>

			{/*Pictures Section*/}
			<div style={styles.imagesWrapper}>
				<p style={styles.pictureInfo}>Pictures</p>
				<img style={styles.images} src={`${this.props.profile.imagesFolder}image1.jpg`} alt='placeholder alt text' />
				<img style={styles.images} src={`${this.props.profile.imagesFolder}image2.jpg`} alt='placeholder alt text' />
				<img style={styles.images} src={`${this.props.profile.imagesFolder}image3.jpg`} alt='placeholder alt text' />
			</div>


			</div>

		</div>

	)
}}

export default ProfileInfo;