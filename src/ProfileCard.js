import React from 'react';

export default class ProfileCard extends React.Component {
	constructor(props) {
		super(props);

		this.setProfile = this.setProfile.bind(this);
	}

	setProfile() {
		if (this.props.setProfile !== undefined) {
			this.props.setProfile(this.props.profile);
		}
	}

	render(){
		const styles = {
  			
  			card: {
					cursor: this.props.setProfile === undefined ? 'auto' : 'pointer',
    			backgroundColor: "#f28001",
  				height: "20rem",
  				borderRadius: "5px",
  				display: "flex",
  				flexDirection: "column",
					alignItems: "center",
  				color: "white",
  				boxShadow: "0 10px 50px #440e08"
  			},
  			
  			avatar: {
    			height: "120px",
  				width: "120px",
					borderRadius: "50%",
  				border: "2px solid black",
  				marginTop: "20px",
  				boxShadow: "0 10px 40px #723900"
  			},
  			
  			name: {
    			marginTop: "30px",
    			fontSize: "1.5em"
  			},
  			
  			gridContainer: {
  				display: "grid",
  				gridTemplateColumns: "1fr 1fr",
  				gridGap: "10px",
  			},

  			age: {
  				textAlign: "center",
  				fontSize: "14px"
  			},
				
  			race: {
  				textAlign: "center",
  				paddingLeft: "10px",
  				fontSize: "14px",
  				fontStyle: "italic"
  			}

  		}

  		return(	
			<div style={styles.card} onClick={this.setProfile}>
				<img
					src = {`${this.props.profile.imagesFolder}profile.jpg`}
					alt={`${this.props.profile.name}'s profile pic.`}
					style={styles.avatar} 
				/>
				<p style={styles.name}>
				{this.props.profile.name}
				</p>
				<div style={styles.gridContainer}>
					<div>
						<p style={styles.race}>{this.props.profile.race}</p>
					</div>
					<div>
						<p style={styles.age}>Age: {this.props.profile.age} years old</p>
					</div>
				</div>
			</div>
		);

 }
}
