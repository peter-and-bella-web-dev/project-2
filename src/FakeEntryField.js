import React from 'react';

export default class FakeEntryField extends React.Component {
  constructor(props) {
    super(props);
    this.state = { value: '', index: 0 };
    this.fillInData = this.fillInData.bind(this);
  }

  fillInData() {
    if (this.state.index < this.props.content.length) {
      this.setState({
        value: this.state.value + this.props.content[this.state.index],
        index: this.state.index + 1,
      });
    }
  }

  render() {
    const styles = {
      div: {
        width: '100%',
        height: '100%',
      },

      label: {
        fontSize: '1.6em',
      },

      input: {
        fontFamily: 'Lato',
        fontSize: '1.15em',
      },

      textarea: {
        resize: 'none',
        width: '95%',
        height: '82%',
        margin: 'auto',
        fontFamily: 'Lato',
        fontSize: '1.2em',
        lineHeight: '1.6em',
      },
    };
    if (this.props.size === 'small') {
      return (
        <div>
          <label style={styles.label} htmlFor='input'>{this.props.label}</label>
          <input style={styles.input} type='text' id='input' name='input' value={this.state.value} onKeyDown={this.fillInData}></input>
        </div>
      );
    } else {
      return (
        <div style={styles.div}>
          <label style={styles.label} htmlFor='input'>{this.props.label}</label>
          <textarea style={styles.textarea} id='input' name='input' value={this.state.value} onKeyDown={this.fillInData}></textarea>
        </div>
      )
    }
  }
}