import React from 'react';

// profiles json
import profiles from './profiles.json';

//components
import Title from './Title';
import ProfilesList from './ProfilesList';
import ProfileInfo from './ProfileInfo';


export default class Container extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			selectedProfile: undefined,
		};

		this.setProfile = this.setProfile.bind(this);
	}

	setProfile(profile) {
		this.setState({
			selectedProfile: profile,
		});
	}

	render() {
		const styles = {
			container: {
				textAlign: 'center',
				maxWidth: '1920px',
				height: '900px',
				margin: '0 auto',
				padding: '40px 25px',
				fontFamily: 'Lato',
			}
		};
		if (this.state.selectedProfile === undefined) {
			return(
				<div style={styles.container} >
					<Title />
					<ProfilesList setProfile={this.setProfile} profiles={profiles} />
				</div>
			);
		} else {
			return(
				<div style={styles.container} >
					<Title />
					<ProfileInfo profile={this.state.selectedProfile} />
				</div>
			);
		}
	}
};
