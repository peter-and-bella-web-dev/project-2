import React from 'react';

// components
import ProfileCard from './ProfileCard'


export default class ProfilesList extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			profilesToRender: [],
		};
	}

	componentDidMount() {
		const numProfiles = 3;
		const profiles = this.props.profiles.slice();
		const profilesToRender = [];
		for (let i = 0; i < numProfiles; i++) {
			const index = Math.floor(Math.random() * profiles.length);
			const profile = profiles[index];
			if (profile !== undefined) {
				profilesToRender.push(profile);
				profiles.splice(index, 1);
			}
		}
		console.log(profilesToRender);
		this.setState({ profilesToRender });
	}

	render(){
		const styles = {
  		container: {
				display: "grid",
  			gridTemplateColumns: "300px 300px 300px",
  			gridGap: "50px",
  			justifyContent: "center",
  			alignItems: "center",
  			height: "65svh",
 				fontFamily: "Lato",
	 			marginTop: "100px"
  		}
  	}
		return(
			<div style={styles.container}>
				{this.state.profilesToRender.map(profile => <ProfileCard setProfile={this.props.setProfile} profile={profile} />)}
			</div>
		)
	}
};
