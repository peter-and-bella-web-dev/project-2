import React from 'react';

//components
import Title from './Title';
import ProfilesList from './ProfilesList';


export default class Container extends React.Component {
	render() {
		const styles = {
			container: {
				textAlign: 'center',
				maxWidth: '1920px',
				height: '900px',
				margin: '0 auto',
				border: '1px solid #e6e6e6',
				padding: '40px 25px',
				fontFamily: 'Lato',
			}
		};
		return(
			<div style={styles.container} >
				<Title />
				<ProfilesList />
			</div>
		);
	}
};
