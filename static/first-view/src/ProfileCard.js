import React from 'react'

export default class ProfileCard extends React.Component {
	render(){
		const styles = {
  			
  			container: {
    			display: "grid",
  				gridTemplateColumns: "300px 300px 300px",
  				gridGap: "50px",
  				justifyContent: "center",
  				alignItems: "center",
  				height: "65svh",
					fontFamily: "Lato",
					marginTop: "100px"
  			},
  			
  			card: {
    			backgroundColor: "#f28001",
  				height: "20rem",
  				borderRadius: "5px",
  				display: "flex",
  				flexDirection: "column",
					alignItems: "center",
  				boxShadow: "rgba(0, 0, 0, 0.7)",
  				color: "white",
  				boxShadow: "0 10px 50px #440e08"
  			},
  			
  			avatar: {
    			height: "120px",
  				width: "120px",
					borderRadius: "50%",
  				border: "2px solid black",
  				marginTop: "20px",
  				boxShadow: "0 10px 40px #723900"
  			},
  			
  			name: {
    			marginTop: "30px",
    			fontSize: "1.5em"
  			},
  			
  			gridContainer: {
  				display: "grid",
  				gridTemplateColumns: "1fr 1fr",
  				gridGap: "10px",
  			},

  			age: {
  				textAlign: "center",
  				fontSize: "14px"
  			},
				
  			race: {
  				textAlign: "center",
  				paddingLeft: "10px",
  				fontSize: "14px",
  				fontStyle: "italic"
  			}

  		}

  		return(	
		<div>
			<div style={styles.card}>
				<img
				src = {this.props.image}
				alt="Dirk Dragon Profile Picture"
				style={styles.avatar} 
				/>
				<p style={styles.name}>
				{this.props.name}
				</p>
				<div style={styles.gridContainer}>
					<div>
						<p style={styles.race}>{this.props.race}</p>
					</div>
					<div>
						<p style={styles.age}>Age: {this.props.age} years old</p>
					</div>
				</div>
			</div>
		</div>
		);

 }
}
