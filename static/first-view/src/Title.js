import React from 'react';

export default class Title extends React.Component {
	render(){
		const headerStyle = {
			fontSize: "50px",
			display: "grid",
			marginTop: "50px",
			fontFamily: "Lato",
			color: "white",
			textShadow: "2px 2px #440e08"

		}

		const h2Style = {
			fontSize: "30px",
			display: "grid",
			fontFamily: "Lato",
			color: "white",
			textShadow: "2px 2px #440e08"
		}
		return(
			<div>
				<h1 style={headerStyle}>Welcome to Lancelot</h1>
				<h2 style={h2Style}>Select a profile</h2>
			</div>
		)
	}
};
