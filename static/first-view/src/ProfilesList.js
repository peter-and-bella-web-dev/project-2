import React from 'react';
import dragon from './images/dragon.jpg';
import gnome from './images/gnome.jpg';
import yeti from './images/yeti.jpg';

// components
import ProfileCard from './ProfileCard'


export default class ProfilesList extends React.Component {
	render(){
		const styles = {
  			container: {
    			display: "grid",
  				gridTemplateColumns: "300px 300px 300px",
  				gridGap: "50px",
  				justifyContent: "center",
  				alignItems: "center",
  				height: "65svh",
 					fontFamily: "Lato",
	 				marginTop: "100px"
  			}
  		}
		return(
			<div style={styles.container}>
			
				{/*Yandrew*/}
				<div> <ProfileCard image={yeti} name='Yandrew Yeti' race='Dinthropoides nivalis' age='123' /> </div>
			
				{/*Gretta*/}
				<div> <ProfileCard image={gnome} name='Gretta Gnome' race='Gernumbli gardensi' age='78' /> </div>
			
				{/*Dirk*/}
				<div> <ProfileCard image={dragon} name='Dirk Dragon' race='Draco petrus silvam' age='405' /> </div>

			</div>
		)
	}
};
