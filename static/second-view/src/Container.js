import React from 'react';

//components
import ProfileInfo from './ProfileInfo';


class Container extends React.Component {
	render(){
		return(
			<div className='container'>
				<ProfileInfo />
			</div>

		)
	}
};

export default Container;