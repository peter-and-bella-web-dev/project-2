import React from 'react'

//components
import DirkCard from './DirkCard';

//images
import placeholder from './images/placeholder.jpg'

class ProfileInfo extends React.Component {
	render(){
		const styles = {
  			
  			container: {
    			background: "white",
  				borderRadius: "50px",
  				width: "95%",
  				minHeight: "90%",
					paddingTop: '5px',
  			},
  			
  			cardContainer: {
					margin: "20px",
					fontSize: "30px",
  			},

  			informationContainer: {
					boxSizing: 'border-box',
					padding: '10px',
  				display: "flex",
					flexFlow: 'row wrap',
					justifyContent: 'space-around',
  			},
  			
  			aboutMe: {
					width: '45%',
					height: '400px',
  				borderWidth: "2px",
  				borderStyle: "solid",
  				borderColor: "black",
  				textAlign: "left",
  				fontSize: "20px",
  				fontWeight: "bold",
  				paddingLeft: "10px",
  			},

				aboutMeInfo: {
					margin: '10px',
				},

				imagesWrapper: {
					width: '100%',
				},

  			pictureInfo: {
  				width: "50%",
  				marginLeft: "20px",
  				textAlign: "left",
  				fontSize: "20px",
  				fontWeight: "bold",
  				marginTop: "50px",
  				paddingLeft: "10px",
  			},

				images: {
					width: "90%",
					maxWidth: '240px',
					height: "auto",
					margin: '12px',
				},

				myDetails: {
					width: "40%",
					borderWidth: "2px",
					borderStyle: "solid",
					textAlign: "left",
					fontSize: "20px",
					fontWeight: "bold",
					paddingLeft: "10px",
				},

				list: {
					lineHeight: "60px",
					marginLeft: "40px",
				}
  		
	}
	return(
		<div style={styles.container}>

			{/*Load in character*/}
			<div style={styles.cardContainer}>	
				<DirkCard />
			</div>
			
			<div style={styles.informationContainer}>
			{/*About Me Section*/}
			<div style={styles.aboutMe}>
				<p style={styles.aboutMeInfo}>About Me</p>
			</div>
			
			{/*My Details Section*/}
			<div style={styles.myDetails}>
				<p>My Details</p>
				<ul style={styles.list}>
					<li style={styles.list}>Height: </li>
					<li style={styles.list}>Birthday: </li>
					<li style={styles.list}>Diet: </li>
					<li style={styles.list}>Interested In: </li>
				</ul>
			</div>

			{/*Pictures Section*/}
			<div style={styles.imagesWrapper}>
				<p style={styles.pictureInfo}>Pictures</p>
				<img style={styles.images} src={placeholder}/>
				<img style={styles.images} src={placeholder}/>
				<img style={styles.images} src={placeholder}/>
			</div>


			</div>

		</div>

	)
}}

export default ProfileInfo;