import React from 'react';

// components
import Container from './Container';

export default class App extends React.Component {
  render() {
    return (
      <Container />
    );
  }
}
