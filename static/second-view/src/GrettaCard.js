import React from 'react'
import ReactDOM from 'react-dom';
import gnome from './images/gnome.jpg'


class GrettaCard extends React.Component {
	render(){
		const styles = {
  			
  			
  			card: {
    			backgroundColor: "#f28001",
  				height: "20rem",
  				borderRadius: "5px",
  				display: "flex",
  				flexDirection: "column",
 				alignItems: "center",
  				boxShadow: "rgba(0, 0, 0, 0.7)",
  				color: "white",
  				boxShadow: "0 1px 30px #440e08"
  			},
  			
  			avatar: {
    			height: "120px",
  				width: "120px",
 				borderRadius: "50%",
  				border: "2px solid black",
  				marginTop: "20px",
  				boxShadow: "0 10px 40px #723900"
  			},
  			
  			name: {
    			marginTop: "30px",
    			fontSize: "1.5em"
  			},
  			
  			gridContainer: {
  				display: "grid",
  				gridTemplateColumns: "1fr 1fr",
  				gridGap: "10px",
  			},

  			age: {
  				textAlign: "center",
  				fontSize: "14px"
  			},
  			race: {
  				textAlign: "center",
  				paddingLeft: "10px",
  				fontSize: "14px",
  				fontStyle: "italic"
  			}

  		}

  		return(	
		<div>
			<div style={styles.card}>
				<img
				src = {gnome}
				alt="Gretta Gnome Profile Picture"
				style={styles.avatar} 
				/>
			<p style={styles.name}>Gretta Gnome</p>
			<div style={styles.gridContainer}>
				<div>
					<p style={styles.race}>Gernumbli gardensi</p>
				</div>
				<div>
					<p style={styles.age}>Age: 78 years old</p>
				</div>
			</div>
		</div>
	</div>
	)

 }
};

export default GrettaCard;